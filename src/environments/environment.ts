// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBRUr7R3RhpMfuYHf0Fin7kcaw_X9CeOak",
    authDomain: "catalogueproduct-e86f5.firebaseapp.com",
    projectId: "catalogueproduct-e86f5",
    storageBucket: "catalogueproduct-e86f5.appspot.com",
    messagingSenderId: "332640608959",
    appId: "1:332640608959:web:cbcfc03ed6438321addc0a",
    measurementId: "G-J8TY2PF754"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
