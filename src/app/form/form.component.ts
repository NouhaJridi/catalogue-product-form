import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient , HttpRequest} from '@angular/common/http';
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent{
        productForm : FormGroup = this.formBuilder.group(
      {
        institution: ['', Validators.required],
        institutionCode: ['', Validators.required],
        productCode: [
          '',
          [
            Validators.required,
            Validators.pattern('^[a-zA-Z0-9 \'\-]+$'),
            Validators.maxLength(6)
          ]
        ],
        productName: ['',Validators.required],
        productLevel: ['',Validators.required],
        version:null,
        derivation: null, 
        description:null, 
        duplication:null, 
        parentInstitution:null, 
        parentProductCode:null, 
        parentProductName:null, 
        parentProductVersion:null, 
        productType:null, 
        status:null, 
        validityEndDate:null, 
        validityStartDate:null,
        statusDate:null,    
      }
    );
  constructor(private formBuilder: FormBuilder, private http: HttpClient) { }

get f()
{
    return this.productForm.controls;
}

  onSubmit() {



      var data = {
        institution:this.productForm.get('institution')!.value,
        institutionCode:this.productForm.get('institutionCode')!.value,
        productCode:this.productForm.get('productCode')!.value,
        productName:this.productForm.get('productName')!.value,
        productLevel:this.productForm.get('productLevel')!.value,
        version:this.productForm.get('version')!.value,
        derivation:this.productForm.get('derivation')!.value, 
        description:this.productForm.get('description')!.value, 
        duplication:this.productForm.get('duplication')!.value, 
        parentInstitution:this.productForm.get('parentInstitution')!.value, 
        parentProductCode:this.productForm.get('parentProductCode')!.value, 
        parentProductName:this.productForm.get('parentProductName')!.value, 
        parentProductVersion:this.productForm.get('parentProductVersion')!.value, 
        productType:this.productForm.get('productType')!.value, 
        status:this.productForm.get('status')!.value, 
        validityEndDate:this.productForm.get('validityEndDate')!.value, 
        validityStartDate:this.productForm.get('validityStartDate')!.value,
        statusDate:this.productForm.get('statusDate')!.value,  
      }

      console.log(this.productForm.get('status')!.value)
    const req = new HttpRequest('POST', 'http://localhost:8080/api/products', data, {
      reportProgress: false,
      responseType: 'json'
    });
  console.log(req);
    this.http.request(req).subscribe(
      (response) => console.log(response),
      (error) => console.log(error)
    )


}

}
