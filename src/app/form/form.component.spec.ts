import { async , ComponentFixture , TestBed } from '@angular/core/testing';
import { FormComponent } from './form.component';
import { By } from '@angular/platform-browser';
import { ButtonComponent } from '../button/button.component';
import {
    FormGroup,
    ReactiveFormsModule
} from '@angular/forms';

describe('FormComponent', () => {
  let component: FormComponent;
  let fixture: ComponentFixture<FormComponent>;

  beforeEach(async() => {
    await TestBed.configureTestingModule({
      imports: [ ReactiveFormsModule  ],
      declarations: [ FormComponent, ButtonComponent]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  /*it('Form should have valid', async() => {
    const component = await render(FormComponent);
    component.type(component.getByTestId(''));  
  });*/

  it('[form-Check] - should check form is valid or not if no values entered', () => {
  expect(component.productForm.valid).toBeFalsy();
}); 

  function setFormValues(fromData: any) {
 component.productForm.controls['institution'].setValue(fromData.institution);
 component.productForm.controls['institutionCode'].setValue(fromData.institutionCode);
 component.productForm.controls['productCode'].setValue(fromData.productCode);
 component.productForm.controls['productName'].setValue(fromData.productName);
 component.productForm.controls['productLevel'].setValue(fromData.productLevel);
}

it('[form-Check] - should check form is valid or not when values entered', () => {
 expect(component.productForm.valid).toBeFalsy();
 const data = {
   institution: 'xyz',
   institutionCode: 'xyz',
   productCode:'458h',
   productName:'cgvh',
   productLevel:'25',
 };
 setFormValues(data);
 expect(component.productForm.valid).toBeTruthy();
})

it('[institution-Check] - should check institution field validity', () => {
  let institution = component.productForm.controls['institution']; 
  expect(institution.valid).toBeFalsy();
  institution.setValue('instructionu1');
  expect(institution.errors).toBeNull();
  expect(institution.valid).toBeTruthy();
});

it('[institution code-Check] - should check institution code field validity', () => {
  let institutionCode = component.productForm.controls['institutionCode'];
  expect(institutionCode.valid).toBeFalsy();
  institutionCode.setValue('codeInstruction');
  expect(institutionCode.errors).toBeNull();
  expect(institutionCode.valid).toBeTruthy();
});

it('[product code-Check] - should check product code field validity', () => {
  let productCode = component.productForm.controls['productCode'];
  expect(productCode.valid).toBeFalsy();
});

it('[product name-Check] - should check product name is required', () => {
  let productName = component.productForm.controls['productName'];
  expect(productName.valid).toBeFalsy();
  productName.setValue('ProductName');
  expect(productName.errors).toBeNull();
  expect(productName.valid).toBeTruthy();
});

it('[product level-Check] - should check product level is required', () => {
  let productLevel = component.productForm.controls['productLevel'];
  expect(productLevel.valid).toBeFalsy();
  productLevel.setValue('L1');
  expect(productLevel.errors).toBeNull();
  expect(productLevel.valid).toBeTruthy();
});

it('[institution-Check] - should check institution is required', () => {
    let institution = component.productForm.controls['institution'];
    expect(institution.errors!['required']).toBeTruthy(); 
  });

it('[institution code-Check] - should check institution code is required', () => {
    let institutionCode = component.productForm.controls['institutionCode'];
    expect(institutionCode.errors!['required']).toBeTruthy(); 
  });


it('[product name-Check] - should check products name is required', () => {
    let productName = component.productForm.controls['productName'];
    expect(productName.errors!['required']).toBeTruthy(); 
  });

it('[product code-Check] - should check products code is invalid',()=>{
  let productCode = component.productForm.controls['productCode'];
  expect(productCode.valid).toBeFalsy();
  expect(productCode.pristine).toBeTruthy();
  expect(productCode.errors!['required']).toBeTruthy();
  productCode.setValue('§jkl');
  expect(productCode.errors!.pattern).toBeTruthy();
  productCode.setValue('jknhgio');
  expect(productCode.errors!['maxlength']).toBeTruthy();
});

it('[product code-Check] - should check users correct products code is entered',()=>{
  let productCode = component.productForm.controls['productCode'];
  productCode.setValue('kl4n');
  expect(productCode.errors!).toBeNull();
});
  it('[Form-Submit] - should check form is submitted',() =>{
    //check form is invalid 
    expect(component.productForm.valid).toBeFalsy();
    //let btn = fixture.debugElement.query(By.css('#btnSubmit'));
    //console.log(btn);
    //check button is disabled
    //expect(btn.nativeElement.disabled).toBeTruthy();
    const data = {
      institution: 'xyz',
      institutionCode: 'xyz',
      productCode:'458h',
      productName:'cgvh',
      productLevel:'25',
    };
    setFormValues(data);
    fixture.detectChanges();

    //check button is enabled
   // expect(btn.nativeElement.disabled).toBeFalsy();

    component.onSubmit();
    fixture.detectChanges();

  });


});
